# Piotr Morawski task

## Live

Project is deployed on Neltify, https://teacode-task.netlify.app/

## Run locally

1. Clone repo
2. Then:
```bash
$ npm install
```
3. Then:
```bash
$ npm run develop
```
4. Go to your localhost:8001