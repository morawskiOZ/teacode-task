import * as React from 'react'
import ContactApp from 'src/components/ContactApp'
import Helmet from 'react-helmet'

const Home = (): React.ReactElement => {
  return (
    <>
      <Helmet>
        <link
          href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;500;700&display=swap"
          rel="stylesheet"
        />
      </Helmet>
      <ContactApp />
    </>
  )
}
export default Home
