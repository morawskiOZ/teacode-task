import {
  useEffect,
  useReducer,
  useState,
  Dispatch,
  SetStateAction,
  Reducer,
} from 'react'
import {
  DataFetchActions,
  dataFetchReducer,
  Data,
  ActionObject,
} from './dataFetchReducer'

export const useDataApi = <D>(
  initialUrl: string,
  initialData: D,
): [Data<D>, Dispatch<SetStateAction<string>>] => {
  const [url, setUrl] = useState(initialUrl)

  const [state, dispatch] = useReducer<
    Reducer<Data<D>, ActionObject<DataFetchActions, D>>
  >(dataFetchReducer, {
    isLoading: true,
    isError: false,
    data: initialData,
  })

  useEffect(() => {
    let didCancel = false

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const fetchData = async (): Promise<any> => {
      dispatch({ type: DataFetchActions.FETCH_INIT })

      try {
        const result = await fetch(url, {
          headers: { accept: 'Accept: application/json' },
        })
        const json = await result.json()
        if (!didCancel) {
          dispatch({
            type: DataFetchActions.FETCH_SUCCESS,
            payload: json.data,
          })
        }
      } catch (error) {
        if (!didCancel) {
          dispatch({ type: DataFetchActions.FETCH_FAILURE })
        }
      }
    }

    fetchData()

    return (): void => {
      didCancel = true
    }
  }, [url])

  return [state, setUrl]
}
