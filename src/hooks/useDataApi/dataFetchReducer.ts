// eslint-disable-next-line @typescript-eslint/no-explicit-any
export type ActionObject<T, P = any> = { type: T; payload?: P }

export enum DataFetchActions {
  FETCH_INIT = 'fetch_init',
  FETCH_SUCCESS = 'fetch_success',
  FETCH_FAILURE = 'fetch_failure',
}

export interface Data<D> {
  isLoading: boolean
  isError: boolean
  data: D
}

export const dataFetchReducer = <S>(
  state: Data<S>,
  action: ActionObject<DataFetchActions, S>,
): Data<S> => {
  switch (action.type) {
    case DataFetchActions.FETCH_INIT:
      return {
        ...state,
        isLoading: true,
        isError: false,
      }
    case DataFetchActions.FETCH_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.payload,
      }
    case DataFetchActions.FETCH_FAILURE:
      return {
        ...state,
        isLoading: false,
        isError: true,
      }
    default:
      return state
  }
}
