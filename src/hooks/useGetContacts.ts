import { useEffect } from 'react'
import { Data } from './useDataApi/dataFetchReducer'
import { useDataApi } from './useDataApi/useDataApi'

export interface Contact {
  id: number
  first_name: string
  last_name: string
  email: string
  gender: string
  avatar: string
}

export const useGetContacts = (): Data<Contact[]> => {
  const url = '/.netlify/functions/contact-fetch'
  const [state, setUrl] = useDataApi<Contact[]>(url, [] as Contact[])

  useEffect(() => {
    setUrl(url)
  }, [])

  return state
}
