import { makeStyles } from '@material-ui/core/styles'
import * as React from 'react'
import { ReactElement, memo } from 'react'
import { Typography } from '@material-ui/core'

const useStyles = makeStyles({
  title: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    background:
      'linear-gradient(90deg, #48ded4 0%, #a026bf 20%, #e82c75 60%, #FFC40E 85%, #48ded4 95%)',
    color: 'white',
    height: 48,
    width: '100%',
  },
  text: {
    fontSize: 20,
    textTransform: 'uppercase',
    fontWeight: 700,
  },
})

export const Title = memo(
  (): ReactElement => {
    const classes = useStyles()
    return (
      <div className={classes.title}>
        <Typography className={classes.text}>Contacts</Typography>
      </div>
    )
  },
)
