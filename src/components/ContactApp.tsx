import * as React from 'react'
import { useReducer, useState, useCallback } from 'react'
import { ContactInput } from 'src/components/ContactInput'
import { Contacts } from 'src/components/Contacts'
import { Contact, useGetContacts } from 'src/hooks/useGetContacts'
import {
  ContactsActions,
  contactsReducer,
  ContactsState,
} from 'src/state/contacts/contactsReducer'
import { Title } from './Title'

const ContactApp = (): React.ReactElement => {
  const initialState: ContactsState = {
    contacts: null as Contact[],
    filteredContacts: null as Contact[],
    isFilteringFinished: false,
    activeContacts: [],
  }
  const [contactsState, dispatch] = useReducer(contactsReducer, initialState)
  const { data, isError, isLoading } = useGetContacts()
  const [isFiltering, setIsFiltering] = useState(false)

  React.useEffect(() => {
    if (contactsState.isFilteringFinished) {
      setIsFiltering(false)
      dispatch({ type: ContactsActions.CLEAR_FILTERING_FLAG })
    }
  }, [contactsState.isFilteringFinished])

  React.useEffect(() => {
    // eslint-disable-next-line no-console
    console.log('active contacts ids:', ...contactsState.activeContacts)
  }, [contactsState.activeContacts.length])

  React.useEffect(() => {
    setIsFiltering(true)
    if (!isLoading && !isError) {
      dispatch({ type: ContactsActions.LOAD_CONTACTS, payload: { data } })
    }
  }, [isLoading, isError])

  const onSearch = useCallback((phrase: string): void => {
    setIsFiltering(true)
    // Timeout 0 to update ui with Loading status before making calc
    setTimeout(() => {
      if (phrase) {
        dispatch({
          type: ContactsActions.FILTER_CONTACTS,
          payload: { data, phrase },
        })
      } else {
        dispatch({
          type: ContactsActions.CLEAR_FILTERS,
        })
      }
    }, 0)
  }, [])

  return (
    <>
      <Title />
      <ContactInput onSearch={onSearch} />
      <Contacts
        contacts={contactsState.filteredContacts}
        isError={isError}
        isLoading={isLoading}
        isFiltering={isFiltering}
        dispatch={dispatch}
      />
    </>
  )
}
export default ContactApp
