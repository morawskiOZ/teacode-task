import { Grid, Typography } from '@material-ui/core'
import * as React from 'react'
import { ReactElement } from 'react'
import { ActionObject } from 'src/hooks/useDataApi/dataFetchReducer'
import {
  ContactsActions,
  ContactsPayload,
} from 'src/state/contacts/contactsReducer'
import CircularProgress from '@material-ui/core/CircularProgress'
import { Contact } from '../hooks/useGetContacts'
import { ContactRow } from './ContactRow'

interface Props {
  contacts: Contact[]
  isLoading: boolean
  isFiltering: boolean
  isError: boolean
  dispatch: React.Dispatch<ActionObject<ContactsActions, ContactsPayload>>
}

export const Contacts = ({
  contacts,
  isError,
  isLoading,
  isFiltering,
  dispatch,
}: Props): ReactElement => {
  const renderContacts = (): ReactElement[] => {
    return contacts.map(contact => {
      return (
        <ContactRow
          avatar={contact.avatar}
          first_name={contact.first_name}
          last_name={contact.last_name}
          email={contact.email}
          id={contact.id}
          gender={contact.gender}
          key={`${contact.id}${contact.last_name}`}
          dispatch={dispatch}
        />
      )
    })
  }

  if (isLoading || isFiltering) {
    return (
      <Grid
        container
        direction="row"
        justify="center"
        alignItems="center"
        spacing={3}
      >
        <CircularProgress disableShrink />
      </Grid>
    )
  }

  if (isError) {
    return (
      <Typography align="center">
        Error occurred while fetching contacts, please try later
      </Typography>
    )
  }

  if (!contacts?.length)
    return <Typography align="center">No contacts found</Typography>

  return (
    <Grid container direction="row" spacing={0} justify="flex-start">
      {renderContacts()}
    </Grid>
  )
}
