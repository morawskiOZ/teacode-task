/* eslint-disable @typescript-eslint/camelcase */
import { Typography } from '@material-ui/core'
import Avatar from '@material-ui/core/Avatar'
import ButtonBase from '@material-ui/core/ButtonBase'
import Checkbox from '@material-ui/core/Checkbox'
import Grid from '@material-ui/core/Grid'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import * as React from 'react'
import { ReactElement, useState, useCallback, memo } from 'react'
import { Contact } from 'src/hooks/useGetContacts'
import { ActionObject } from 'src/hooks/useDataApi/dataFetchReducer'
import {
  ContactsActions,
  ContactsPayload,
} from 'src/state/contacts/contactsReducer'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    avatarWrapper: {
      marginLeft: 20,
      width: 75,
    },
    contact: {
      padding: 8,
      justifyContent: 'flex-start',
      backgroundColor: theme.palette.grey[100],
      borderBottom: `1px solid ${theme.palette.grey[400]}`,
      borderLeft: `1px solid ${theme.palette.grey[400]}`,
    },
    checkbox: {
      width: 75,
    },
  }),
)

interface Props extends Contact {
  dispatch: React.Dispatch<ActionObject<ContactsActions, ContactsPayload>>
}

export const ContactRow = memo(
  ({ avatar, first_name, last_name, dispatch, id }: Props): ReactElement => {
    const classes = useStyles()
    const [isChecked, setIsChecked] = useState(false)

    const clickHandler = useCallback(() => {
      setIsChecked(state => {
        if (!state) {
          dispatch({
            type: ContactsActions.ADD_ACTIVE_CONTACT,
            payload: { id },
          })
        } else {
          dispatch({
            type: ContactsActions.DELETE_ACTIVE_CONTACT,
            payload: { id },
          })
        }
        return !state
      })
    }, [])

    return (
      <Grid
        item
        container
        direction="row"
        classes={{ root: classes.contact }}
        component={ButtonBase}
        onClick={clickHandler}
        xs={12}
        md={6}
        xl={3}
      >
        <Grid item xs={3} sm={2} container>
          <div className={classes.avatarWrapper}>
            <Avatar alt="avatar" src={avatar} />
          </div>
        </Grid>
        <Grid item xs={6} sm={8} container>
          <Typography>
            {first_name} {last_name}
          </Typography>
        </Grid>
        <Grid item xs={2} container>
          <Checkbox checked={isChecked} classes={{ root: classes.checkbox }} />
        </Grid>
      </Grid>
    )
  },
)
