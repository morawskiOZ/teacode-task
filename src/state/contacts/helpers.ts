import { Contact } from 'src/hooks/useGetContacts'

export const sortAlphabetically = (contacts: Contact[]): Contact[] => {
  if (!contacts || !Array.isArray(contacts)) return []

  return [...contacts].sort((a, b) =>
    a?.last_name?.toLowerCase().localeCompare(b?.last_name?.toLowerCase()),
  )
}

export const filterContacts = (
  contacts: Contact[],
  phrase: string,
): Contact[] => {
  if (!contacts || !Array.isArray(contacts)) return []
  return contacts.filter(contact =>
    contact?.first_name
      ?.toLowerCase()
      .concat(' ', contact?.last_name?.toLowerCase())
      .includes(phrase.toLowerCase()),
  )
}
