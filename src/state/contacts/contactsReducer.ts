import { Contact } from 'src/hooks/useGetContacts'
import { ActionObject } from 'src/hooks/useDataApi/dataFetchReducer'
import { sortAlphabetically, filterContacts } from './helpers'

export enum ContactsActions {
  LOAD_CONTACTS = 'fetch_contacts',
  FILTER_CONTACTS = 'filter_contacts',
  CLEAR_FILTERS = 'clear_filters',
  CLEAR_FILTERING_FLAG = 'clear_filtering_flag',
  ADD_ACTIVE_CONTACT = 'add_active_contact',
  DELETE_ACTIVE_CONTACT = 'delete_filtering_flag',
}

export interface ContactsPayload {
  data?: Contact[]
  phrase?: string
  id?: number
}

export interface ContactsState {
  contacts: Contact[]
  filteredContacts: Contact[]
  isFilteringFinished: boolean
  activeContacts: number[]
}

export const contactsReducer = (
  state: ContactsState,
  action: ActionObject<ContactsActions, ContactsPayload>,
): ContactsState => {
  switch (action.type) {
    case ContactsActions.LOAD_CONTACTS: {
      const sortedContacts = sortAlphabetically(action.payload.data)
      return {
        ...state,
        contacts: sortedContacts,
        filteredContacts: sortedContacts,
        isFilteringFinished: true,
      }
    }
    case ContactsActions.FILTER_CONTACTS:
      return {
        ...state,
        filteredContacts: filterContacts(state.contacts, action.payload.phrase),
        isFilteringFinished: true,
      }
    case ContactsActions.CLEAR_FILTERS:
      return {
        ...state,
        filteredContacts: state.contacts,
        isFilteringFinished: true,
      }
    case ContactsActions.CLEAR_FILTERING_FLAG:
      return {
        ...state,
        isFilteringFinished: false,
      }
    case ContactsActions.ADD_ACTIVE_CONTACT:
      return {
        ...state,
        activeContacts: [...state.activeContacts, action.payload.id],
      }
    case ContactsActions.DELETE_ACTIVE_CONTACT:
      return {
        ...state,
        activeContacts: state.activeContacts.filter(
          id => id !== action.payload.id,
        ),
      }

    default:
      return state
  }
}
