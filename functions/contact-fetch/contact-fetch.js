/* eslint-disable */
// for a full working demo of Netlify Identity + Functions, see https://netlify-gotrue-in-react.netlify.com/

const fetch = require('node-fetch').default
exports.handler = async function(event, context) {
  if (!context.clientContext && !context.clientContext.identity) {
    return {
      statusCode: 500,
      body: JSON.stringify({
        msg: 'No identity instance detected. Did you enable it?',
      }), // Could be a custom message or object i.e. JSON.stringify(err)
    }
  }
  const { identity, user } = context.clientContext
  try {
    const response = await fetch(
      'https://teacode-recruitment-challenge.s3.eu-central-1.amazonaws.com/users.json',
    )
    if (!response.ok) {
      // NOT res.status >= 200 && res.status < 300
      return { statusCode: response.status, body: response.statusText }
    }
    const data = await response.json()
    const slicedData = data.slice(0, 250)
    return {
      statusCode: 200,
      body: JSON.stringify({ identity, user, data: slicedData }),
    }
  } catch (err) {
    console.log(err) // output to netlify function log
    return {
      statusCode: 500,
      body: JSON.stringify({ msg: err.message }), // Could be a custom message or object i.e. JSON.stringify(err)
    }
  }
}
